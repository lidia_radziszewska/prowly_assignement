# Additional Comments #

Below are some additional comments to showcase the thinking behind creating the test cases.

### Test steps ###

* The cases include the general two-step google search, starting from the google main page. I am aware that now it is often possible to conduct the direct search using the URL address field.
* There are also separate pages for each category, however, they seem to be used less than the main google page, therefore I didn't include them in testing steps.

### Testing environment ###

* As I focused more on the functionality within searching and filtering and no testing environments were specified in the task, the environments in test cases are general. Therefore no devices, browsers, versions of applications were specified.

### Test Data ###

* As cases are often more general and multiple data can be applied to test description I decided to use generic names in testing steps and specify the data for each case separately.
